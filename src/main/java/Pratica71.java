import java.util.*;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

public class Pratica71 {

  public static void main(String[] args) {
      String posicao;
      int numero=0;
      String nome;
      
      Time time = new Time();
    
    Scanner scanner = new Scanner(System.in);
    int numJogadores=0;
    System.out.print("Número de Jogadores: ");
    if (scanner.hasNextInt()) {
        numJogadores = scanner.nextInt();
    } else {
        System.out.println("Digite um número!");
    }
    
    for(int i=0; i<numJogadores; i++){
        
        System.out.print("Numero: ");
        
        if (scanner.hasNextInt()) {
            numero = scanner.nextInt();
        } else {
            System.out.println("Digite um número!");
        }
        System.out.print("Posição: ");
        posicao = scanner.next();
        System.out.print("Nome: ");
        nome = scanner.next();
        
        time.addJogador(posicao, new Jogador(numero, nome));
    }

    // ordem descendente de nome e ascendente de número
    JogadorComparator ordem = new JogadorComparator(true, true, false);
    List<Jogador> timeA = time.ordena(ordem);  
    
    timeA.forEach((jogador) -> {
        System.out.println(jogador.toString());
      });
    
    while(numero > 0){
        System.out.print("Numero: ");
        
        if (scanner.hasNextInt()) {
            numero = scanner.nextInt();
        } else {
            System.out.println("Digite um número!");
        }
        if(numero > 0){
            System.out.print("Posição: ");
            posicao = scanner.next();
            System.out.print("Nome: ");
            nome = scanner.next();
            
/*            if(time.containsValue(numero)){
                
            }
            
            timeA.forEach((Jogador jogador) -> {
                if(jogador.getNumero() == numero){
                    System.out.println(jogador.toString());
                }
                
            });
 */       
            time.addJogador(posicao, new Jogador(numero, nome));
            
            timeA = time.ordena(ordem);
                    
            timeA.forEach((jogador) -> {
                System.out.println(jogador.toString());
            });
        }
    }
  }
}